<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>NetCoupler: Inferring causal pathways between high-dimensional metabolic data and external factors</title>
    <meta charset="utf-8" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# NetCoupler: Inferring causal pathways between high-dimensional metabolic data and external factors
### <p>Luke W. Johnston</p>
<p>Steno Diabetes Center Aarhus, Denmark</p>

---


layout: true

&lt;!-- To the Danish Epidemiological Society --&gt;

&lt;div class="my-header"&gt;&lt;/div&gt;
&lt;div class="my-footer"&gt;
&lt;span&gt;
&lt;img src="../../common/dda-logo.png" alt="DDA", width="75"&gt;
&lt;img src="../../common/sdca-logo.png" alt="SDCA", width="55"&gt;
&lt;a href="https://slides.lwjohnst.com/des/2021-05-21/"&gt;slides.lwjohnst.com/des/2021-05-21&lt;/a&gt;
&lt;/span&gt;
&lt;/div&gt; 





---

# Outline:

- History and background on the analytic problem

- NetCoupler implementation and development

- Examples using NetCoupler

- Current challenges

---

class: middle

# History and background on analytic problem

---

## Amount of data generated is massive

.pull-left[
- -omics type data

- Metabolic biomarkers
]

.pull-right[
- High dimensionality

- Complex networks
]

???

Want to ask questions using this type of data, some way to handle that complexity.

---

## Potential analysis: Dimensionality reduction

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="../../au-ph/2019-08-15/images/pca.png" alt="Reducing number of variables with PCA." width="280" /&gt;
&lt;p class="caption"&gt;Reducing number of variables with PCA.&lt;/p&gt;
&lt;/div&gt;

???

This has the advantage of making things simpler while trying to maximize
variance in the data. Afterward you can do modelling on each principal component.
The disadvantage of this approach is that it loses a lot of information since
the interdependence and connections between variables it not maintained.

---

## Potential analysis: Many regression-type models

.center[
```
O1 = M1 + covariates
O1 = M2 + covariates
...
O1 = M7 + covariates
O1 = M8 + covariates
```
]

???

Some ways you might go about analyzing this data is by running many regression
models, one for each metabolic variable for instance.

This of course has problems since you're simply running a bunch of models and
not taking account of the inherent interdependencies between variables.

---

## Potential analysis: Network analysis

&lt;img src="index_files/figure-html/img-traditional-network-analysis-1.png" width="50%" height="50%" style="display: block; margin: auto;" /&gt;

???

This approach is nice in that you can extract information about the connection
between metabolic variables. But there is no way to incorporate the disease
outcome with this approach and in order to construct the network properly most
methods require you provide a prespecified base network, which you might not know.

---

## But, what if...

- want info about network structure?
--


- don't know the network structure?
--


- have an exposure, metabolites, and outcome?

---

## ... want to know is something like this?

&lt;img src="../../au-ph/2019-08-15/images/network.png" width="75%" style="display: block; margin: auto;" /&gt;

---

class: middle

# (Potential) solution: NetCoupler

---

## Initial development

.pull-left[
- Developed by Clemens Wittenbecher for his 
[PhD thesis](https://publishup.uni-potsdam.de/opus4-ubp/frontdoor/deliver/index/docId/40459/file/wittenbecher_diss.pdf)

- Algorithm that:
    - Finds most likely network structure
    - Allows inclusion of exposure and outcome
    - Identifies causal links from, to, and within the network
]

.pull-right[
![Clemens Wittenbecher](https://avatars3.githubusercontent.com/u/33724052?size=200)
]

---

## Four basic phases of the algorithm

&lt;img src="../../iarc/2020-12-16/images/netcoupler-process.svg" width="90%" style="display: block; margin: auto;" /&gt;

???

A key is that the algorithm is flexible enough to handle different types of
models

---

## Infer (potentially) causal pathways with graphical model output

&lt;img src="../../au-ph/2019-08-15/images/nc-causal-pathways.png" width="85%" style="display: block; margin: auto;" /&gt;

---

## Developing NetCoupler as an R package

--

.center[
&lt;img src="../../iarc/2020-12-16/images/netcoupler-github.png" width="70%" style="display: block; margin: auto;" /&gt;
]

.footnote[
- [github.com/NetCoupler](https://github.com/NetCoupler/NetCoupler)
- **Goal**: Submit to CRAN by mid-2021.
]

???

Met him at EDEG, asked if it was an R package. Started working together after
that.

---

class: middle

# Example projects using NetCoupler

---

## UK Biobank: Metabolic pathways between components of stature and HbA1c

&lt;img src="../../iarc/2020-12-16/images/aim-ukbiobank.svg" height="90%" style="display: block; margin: auto;" /&gt;

---

## UK Biobank characteristics

.pull-left[
- Basics:
    - ~480,000 participants
    - Cross-sectional
    - Stature measures
    - Various demographics
]

.pull-right[
- Metabolic variables:
    - Cholesterol
    - Albumin
    - Alanine Aminotransferase
    - Apolipoprotein A and B
    - Aspartate Aminotransferase
    - C-reactive Protein
    - Gamma Glutamyltransferase
    - HDL and LDL Cholesterol
    - Triglycerides
]

---

## Link between leg length, liver markers, HbA1c

&lt;img src="../../iarc/2020-12-16/images/ukbiobank-netcoupler-results.png" width="60%" style="display: block; margin: auto;" /&gt;

---

class: middle

# Current challenges

---

## Several limitations or improvements

- **Mainly**, performance can be slow
    - E.g. larger data or networks

--

- Untested on larger networks

--

- Untested on non-cross-sectional/time-to-event data

--

- Visualizing can be tricky

--

- Interpreting estimates can be tricky

---

class: middle

# Thanks! Questions?
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
