# SDCA Research Retreat slide-free talk

Time: 10 min \* 100-200/wpm = \~1000-2000 words

Outline:

-   Setting the stage (motivation)

-   Describe the goal/aim

-   Simple walkthrough of a typical User 1 or User 2

-   Our plan, timeline, and where we're at

Imagine that you are a new PI, just starting getting a group and research programme going... or solo researcher or a small research group starts a study to have data for their PhD students, but have limited funds and technical expertise.

Or, you are a large, multi-national/center consortium that wants to keep better track of who's working on what, and how to discover and share data added to the project... or has an aim of widely disseminating their data for maximal, and cost-effective, use by their collaborators and others.

These use cases are the motivations for this new project.
Specifically this project aims is to make a framework for creating an infrastructure for managing health data.
We're still figuring out a better name for it but so far we've been calling it the data infrastructure framework or DIF project.
We're using this framework for the DD2 initiative, so we'll be working with them.

At the basic level, we're making a software product that

1.  makes it easier to find, store, use, and share data for health and biomedical research projects
2.  that abide by best practices in openness, transparency, and rigor,
3.  so that you the researcher can focus on doing the science
4.  rather than struggling with the technical and computational aspects of managing data that fit modern requirements.

If the aim for this project might not be very tangible or concrete, then instead consider these simple questions and how you might answer them:

-   How do store your data and in what file format?

-   Where do you store your data and how do you name the files?

-   How do you keep track of changes that are made to the data?

-   How do you access the data and start analyzing it?

-   For larger multi-center projects, who has which dataset, where are they, and how can you use or access them?

-   How do you or your collaborators, like new PhD students, figure out what specific variables are in the data and what do they mean?

These are some questions that this project either aims to help answer or make irrelevant.

I'll take you through a little walkthrough of some use cases.

-   User 1...

-   User 2...

-   User 4?...

We plan to build this project up in pieces, creating a working prototype of each component as quick as we can, with the first prototype planned to be completed within the next 2 years.
Right now we're in the hiring stage, as we start building up the team.
The full team will be about 5-6 people, with 3 team members working full-time on the project.
