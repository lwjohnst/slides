<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>NetCoupler: Inferring causal pathways between high-dimensional metabolomics data and external factors</title>
    <meta charset="utf-8" />
    <meta name="author" content="Luke W. Johnston" />
    <meta name="author" content="Clemens Wittenbecher" />
    <meta name="author" content="Fabian Eichelmann" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link href="libs/remark-css/useR.css" rel="stylesheet" />
    <link href="libs/remark-css/useR-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="extra.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# NetCoupler: Inferring causal pathways between high-dimensional metabolomics data and external factors
### Luke W. Johnston
### Clemens Wittenbecher
### Fabian Eichelmann

---


layout: true

&lt;!-- To UseR 2021 

Bio:

Diabetes epidemiologist, R teacher, create R packages and tutorials to help researchers do reproducible and open science more easily. Personal site: https://lukewjohnston.com/

Slides are found here: https://slides.lwjohnst.com/user/2021-07-06/

--&gt;

&lt;div class="my-footer"&gt;
Slides: &lt;a href="https://slides.lwjohnst.com/user/2021-07-06/"&gt;slides.lwjohnst.com/user/2021-07-06&lt;/a&gt;
&lt;br&gt;Package: &lt;a href="https://github.com/NetCoupler/NetCoupler/"&gt;github.com/NetCoupler/NetCoupler&lt;/a&gt;
&lt;/div&gt; 



<div>
<style type="text/css">.xaringan-extra-logo {
width: 110px;
height: 128px;
z-index: 0;
background-image: url(../../common/dda-sdca-logos.png);
background-size: contain;
background-repeat: no-repeat;
position: absolute;
top:1em;left:1em;
}
</style>
<script>(function () {
  let tries = 0
  function addLogo () {
    if (typeof slideshow === 'undefined') {
      tries += 1
      if (tries < 10) {
        setTimeout(addLogo, 100)
      }
    } else {
      document.querySelectorAll('.remark-slide-content:not(.title-slide):not(.inverse):not(.hide_logo)')
        .forEach(function (slide) {
          const logo = document.createElement('div')
          logo.classList = 'xaringan-extra-logo'
          logo.href = null
          slide.appendChild(logo)
        })
    }
  }
  document.addEventListener('DOMContentLoaded', addLogo)
})()</script>
</div>



&lt;!--

Instructions:

- Timing:
    - 5 min
    - ~100-150 wpm = 500-750 words total
- Video:
    - Show face (picture-in-picture layout)
    - OBS Studio/Simple Screen Recorder/Zoom
    - Describe headers, graphics
    - Speak each word on slides
    - Describe what and why
- Content:
    - Include speakers notes
    - Include alt-text
    - Include transcript

--&gt;

---

## Designed to identify *potential* causal factors from complex networks

.pull-left[

**Motivation**:

- Moderately high dimensional and complex network data (e.g. metabolomics)
- Derive potential network structure
- Estimate causal pathways:
    - From exposure (e.g. exercise) to network
    - From network to outcome (e.g. diabetes)
    - From exposure to outcome, through the network
]

.pull-right[
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="../../au-ph/2019-08-15/images/network.png" alt="Diagram showing an exposure variable connected by lines to a network of metabolite variables within circles, that are then connected to an outcome variable." width="100%" /&gt;
&lt;p class="caption"&gt;Use NetCoupler to answer questions of this form (M = metabolite).&lt;/p&gt;
&lt;/div&gt;
]

???

Hi everyone, I'm going to be talking about NetCoupler, which is an algorithm and R
package for inferring causal pathways between high-dimensional metabolomics data
and external factors.

We had several motivations for creating NetCoupler, largely because
we wanted to use moderately high dimensional and complex
network data such as from metabolomics and to be able to answer questions about
potential causal pathways that occur through the network.
As illustrated by the diagram, we wanted to know how an exposure like exercise
might influence a network, how a network might influence an outcome like
diabetes, or how an exposure might influence an outcome through the network.

---

## Main features of NetCoupler 


.pull-left[
- Finds most likely network structure

- Can include exposure and/or outcome

- Identifies *potential* causal links from, to, and within the network
]

--

.pull-right[
- Flexible in type of model used (e.g. linear, logistic, cox regression)

- Allows adjusting for confounders and covariates

- Results are designed to be visualized (e.g. with tidygraph/ggraph packages)
]

???

The main features of NetCoupler are that it finds the most likely network
structure, it can include exposure and/or outcome variables, and it can identify
potential causal links involving the metabolic network. 

NetCoupler is also quite flexible in the type of models you can use in it, 
so you could use models like linear or logistic
regression or Cox proportional hazard models. Because these
models can be used, you can also adjust for potential confounding factors that might
bias the results. Since NetCoupler is based on network graphs,
the results are especially designed to be visualized as them too, like with the
packages tidygraph or ggraph.

---

## Four basic phases of the algorithm

&lt;img src="../../iarc/2020-12-16/images/netcoupler-process.svg" title="Diagram with four distinct areas representing the phases.
First phase shows a network of nodes called N commnected by lines that has been derived.
Second phase shows one node, called the index node, as an example of being iteratively selected, 
along with the other nodes it is connected to, called neighbour nodes.
Third phase shows a series of diagrams that are calculated for all combinations of
the index node with its neighbouring nodes. There are eight of these diagrams. Each
diagram represents a model for the next phase.
Fourth phase shows that each model from the previous phase is classified into
direct, ambigious, or no effect. This classification happens separately for 
exposures, called E, and outcomes, called O." alt="Diagram with four distinct areas representing the phases.
First phase shows a network of nodes called N commnected by lines that has been derived.
Second phase shows one node, called the index node, as an example of being iteratively selected, 
along with the other nodes it is connected to, called neighbour nodes.
Third phase shows a series of diagrams that are calculated for all combinations of
the index node with its neighbouring nodes. There are eight of these diagrams. Each
diagram represents a model for the next phase.
Fourth phase shows that each model from the previous phase is classified into
direct, ambigious, or no effect. This classification happens separately for 
exposures, called E, and outcomes, called O." width="90%" style="display: block; margin: auto;" /&gt;

???

The NetCoupler algorithm works in four basic phases, illustrated in this
diagram.

The first phase is that the structure of the metabolic network is derived using
causal structure learning algorithms like the PC-algorithm.

The second phase is where each metabolic variable, called a node, within the
network is iteratively selected and set as the index node. Each connected
neighbouring nodes are then identified and selected. Here, the index node
has three neighbours.

The third phase is where each possible combination of index with neighbouring
node is calculated and used in the model. There are three neighbours here, so
that would be eight different combinations representing eight models.

The fourth phase is taking all these models and linking them with either
an exposure or an outcome variable, as well as any confounding factors. Based on
specific thresholds, the link between exposure or outcome and the index node is
classified as either direct, ambigious, or no effect.

---

## Graphical model output allows visual inference of *potential* pathways

&lt;img src="../../au-ph/2019-08-15/images/nc-causal-pathways.png" title="Diagram showing how the results from NetCoupler might look.
Four lines originate from the exposure node on the left side, 
two thicker lines with arrows indicating direct effects 
and two thinner lines indicating ambigious effects. These four lines connect
to four metabolic variables, marked in red. All other metabolic variables,
as circles with the letter M, are connected with each other by one or two lines.
Four lines originate from four metabolic variables, also marked in red, 
and end at the outcome variable.
Two are the direct effect lines and two are the ambigious lines." alt="Diagram showing how the results from NetCoupler might look.
Four lines originate from the exposure node on the left side, 
two thicker lines with arrows indicating direct effects 
and two thinner lines indicating ambigious effects. These four lines connect
to four metabolic variables, marked in red. All other metabolic variables,
as circles with the letter M, are connected with each other by one or two lines.
Four lines originate from four metabolic variables, also marked in red, 
and end at the outcome variable.
Two are the direct effect lines and two are the ambigious lines." width="85%" style="display: block; margin: auto;" /&gt;

???

The final graphical model output can allow for visual inference of the potential
pathways. For instance, in this example figure, NetCoupler might classify
two direct effects, represented by the thicker lines with arrows, 
and two ambigious effects, represented by the thinner lines, between an exposure or
an outcome and individual metabolic variables. We can then visually trace
the pathway from the exposure, through the metabolic variables, and to the outcome,
and infer that the metabolic variables along this path, marked as red here, may
be along the causal pathway.

---

## Current limitations and areas to improve

.pull-left[
- Conceptual:
    - Tricky to visualize (too many paths and variables)
    - Difficult to interpret output estimates
    - Not suited for pure exploration, should have some theoretical basis
- Modeling:
    - Heavily relies on p-values
    - Only tested on cross-sectional/time-to-event data
]

--

.pull-right[
- Software:
    - Slow performance
    - Untested on networks with &gt;25 variables
    - Probably not sensible for *very* high-dimensional data (e.g. genomics)
]

???

We're actively working on this R package and there are still limitations and 
areas to improve.

Conceptually, figuring out how to meaningfully visualize the results has been
tricky, because you quickly get too much going on. Because of the pre-processing
of the data beforehand, the model estimates can be very difficult to interpret.
We also don't believe this algorithm is suited for pure exploration, as there
should be at least some theoretical basis for potential causal pathways in
your research question.

For modeling, the classification thresholds rely largely on p-values, which can
be problematic and we're working on other types of thresholds. We also have only
tested NetCoupler on cross-sectional or time-to-event data, so don't know how
it would work with other types of data.

Finally, one of the biggest issues is that performance is quite slow. Because of
this, we haven't tested it on networks with larger than 25 or so variables and
we guess it probably isn't sensible to use on very high dimensional data like genomics.

---

class: middle

# Thanks!

???

If you want to see how to use NetCoupler, more detail is on the NetCoupler website
found in the footer. 

Thanks for listening!
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
